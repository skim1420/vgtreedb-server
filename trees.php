<?php

require_once('Rest.inc.php');

class API extends REST {

	public $data = '';
	const DB_SERVER = 'www7.pairlite.com';
	const DB_USER = 'pl1705_2';
	const DB_PASSWORD = 'pl1705_2';
	const DB = 'pl1705_vgtreedb';

	private $db = NULL;

	public function __construct() {
		parent::__construct();
		$this->dbConnect();
	}

	private function dbConnect() {
		$this->db = mysql_connect(self::DB_SERVER, self::DB_USER, self::DB_PASSWORD);
		if ($this->db) {
			mysql_select_db(self::DB, $this->db);
		}
	}

	private function json($data) {
		if (is_array($data)) {
			return json_encode($data);
		}
	}

	public function processApi() {

		$headers = apache_request_headers();
		$apikey = '';

		foreach ($headers as $header => $value) {
			if ($header == "Authorization") {
				$apikey = $value;
			}
		}
		$sql = "SELECT * FROM `keys` WHERE `key` = '".$apikey."'";
		$result = mysql_query($sql, $this->db);
		if (mysql_num_rows($result) < 1) {
			$this->response('Invalid API key', 403);
		}

		$func = strtolower(trim(str_replace('/','',$_REQUEST['method'])));
		if ((int)method_exists($this,$func) > 0) {
			$this->$func();
		} else {
			$this->response('',404);
		}

	}

	private function getTrees() {

		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		$tree_id = urldecode((int)$this->_request['tree_id']);
		$site = urldecode((int)$this->_request['site']);
		$court = urldecode($this->_request['court']);
		$botanical = urldecode($this->_request['botanical']);
		$common = urldecode($this->_request['common']);
		$status = urldecode($this->_request['status']);

		$sql = 'SELECT * FROM trees WHERE 1';
		if ($tree_id) {
			$sql .= ' AND tree_id = '.$tree_id;
		}
		if ($site) {
			$sql .= ' AND site = '.$site;
		}
		if ($court) {
			$sql .= ' AND court = "'.$court.'"';
		}
		if ($botanical) {
			$sql .= ' AND botanical = "'.$botanical.'"';
		}
		if ($common) {
			$sql .= ' AND common = "'.$common.'"';
		}
		if ($status) {
			$sql .= ' AND status = "'.$status.'"';
		}
		$sql .= ' ORDER BY court';

		$result = mysql_query($sql, $this->db);
		if (mysql_num_rows($result) > 0) {
			$trees = array();
			while ($fetch_array = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$trees[] = $fetch_array;
			}
			$this->response($this->json($trees), 200);
		} else {
			$this->response('No matching records',204);
		}
	}

	private function getNearbyTrees() {

		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		$longitude = urldecode((double)$this->_request['longitude']);
		$latitude = urldecode((double)$this->_request['latitude']);

		$sql = 'SELECT * FROM trees WHERE ABS(longitude-'.$longitude.') < 0.001 AND ABS(latitude-'.$latitude.') < 0.001 AND status = "closed"';

		$result = mysql_query($sql, $this->db);
		if (mysql_num_rows($result) > 0) {
			$trees = array();
			while ($fetch_array = mysql_fetch_array($result, MYSQL_ASSOC)) {
				$trees[] = $fetch_array;
			}
			$this->response($this->json($trees), 200);
		}

		$this->response('No matching records',204);

	}

	private function getCourts() {

		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		$botanical = urldecode($this->_request['botanical']);
		$common = urldecode($this->_request['common']);
		$status = urldecode($this->_request['status']);

		$sql = 'SELECT court, COUNT(*) count, MIN(tree_id) thumb_id FROM trees WHERE 1';
		if ($botanical) {
			$sql .= ' AND botanical = "'.$botanical.'"';
		}
		if ($common) {
			$sql .= ' AND common = "'.$common.'"';
		}
		if ($status) {
			$sql .= ' AND status = "'.$status.'"';
		}
		$sql .= ' GROUP BY court ORDER BY 1';

		$result = mysql_query($sql, $this->db);
		$data = array();
		while ($fetch_array = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$data[] = $fetch_array;
		}
		$this->response($this->json($data), 200);

	}

		private function getBotanicals() {

		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		$court = urldecode($this->_request['court']);
		$common = urldecode($this->_request['common']);
		$status = urldecode($this->_request['status']);
		$sql = 'SELECT botanical, COUNT(*) count, MIN(tree_id) thumb_id FROM trees WHERE 1';
		if ($court) {
			$sql .= ' AND court = "'.$court.'"';
		}
		if ($common) {
			$sql .= ' AND common = "'.$common.'"';
		}
		if ($status) {
			$sql .= ' AND status = "'.$status.'"';
		}
		$sql .= ' GROUP BY botanical ORDER BY 1';

		$result = mysql_query($sql, $this->db);
		$data = array();
		while ($fetch_array = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$data[] = $fetch_array;
		}
		$this->response($this->json($data), 200);

	}

	private function getCommons() {

		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		$court = urldecode($this->_request['court']);
		$botanical = urldecode($this->_request['botanical']);
		$status = urldecode($this->_request['status']);
		$sql = 'SELECT common, COUNT(*) count, MIN(tree_id) thumb_id FROM trees WHERE 1';
		if ($court) {
			$sql .= ' AND court = "'.$court.'"';
		}
		if ($botanical) {
			$sql .= ' AND botanical = "'.$botanical.'"';
		}
		if ($status) {
			$sql .= ' AND status = "'.$status.'"';
		}
		$sql .= ' GROUP BY common ORDER BY 1';

		$result = mysql_query($sql, $this->db);
		$data = array();
		while ($fetch_array = mysql_fetch_array($result, MYSQL_ASSOC)) {
			$data[] = $fetch_array;
		}
		$this->response($this->json($data), 200);

	}

	private function setTreeLocation() {
		
		if ($this->get_request_method() != 'GET') {
			$this->response('Invalid HTTP method',406);
		}

		if ($this->_request['tree_id']==NULL || $this->_request['longitude']==NULL || $this->_request['latitude']==NULL) {
			$this->response('Required values missing',406);
		}

		$tree_id = (int)$this->_request['tree_id'];
		$longitude = (double)$this->_request['longitude'];
		$latitude = (double)$this->_request['latitude'];

		$sql = 'SELECT * FROM trees WHERE tree_id = '.$tree_id;
		$result = mysql_query($sql, $this->db);
		if (mysql_num_rows($result) > 0) {
			$sql = 'UPDATE trees SET longitude='.$longitude.',latitude='.$latitude.',status="closed" WHERE tree_id='.$tree_id;
			mysql_query($sql, $this->db);
			$this->response('',200);
		} else {
			$this->response('No matching records',404);
		}


	}

	private function uploadTreePicture() {

		if ($this->get_request_method() != 'POST') {
			$this->response('Invalid HTTP method',406);
		}

		$uploaddir = 'photos/';
		$file_name = $_FILES['userfile']['name'];
		$uploadfile = $uploaddir.$file_name;
		echo $uploadfile;
		move_uploaded_file($_FILES['userfile']['tmp_name'], $uploadfile);

	}

}

$api = new API;
$api->processApi();

?>